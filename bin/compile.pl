#!/usr/bin/env perl

use strict;
use warnings;

use File::Spec;
use File::Basename qw/ dirname /;
use FindBin;

use local::lib File::Spec->catdir( dirname(__FILE__), "..", "local" );

use Compiler::Lexer;
use Compiler::Parser;
use Compiler::CodeGenerator::LLVM;

my $usage       = <<"...";
Usage: ${0} {script.pl} {output.ll}
...

my $filename    = shift @ARGV or die $usage;
my $outfile     = shift @ARGV or die $usage;
my $script      = do {
    open( my $fh, '<', $filename )
        or die "Cannot open file: ${filename}: ${!}";
    my $data = do { local $/; <$fh> };
    close($fh);
    $data;
};

my $lexer       = Compiler::Lexer->new($filename);
my $tokens      = $lexer->tokenize($script);

my $parser      = Compiler::Parser->new();
my $ast         = $parser->parse($tokens);

my $generator   = Compiler::CodeGenerator::LLVM->new();
my $llvm_ir     = $generator->generate($ast);

open( my $fh, '>', $outfile )
    or die "Cannot open filehandle: ${outfile}: ${!}";
print $fh $llvm_ir;
close($fh);

print "Completed!\n";

