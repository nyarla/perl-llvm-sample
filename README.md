perl-llvm-sample
================

Example code for Compiler::CodeGenerator::LLVM

How to Play (OSX)
-----------------

1. install llvm with this command:
  * `$ brew install https://raw.github.com/rhysd/homebrew/13dbf9d3af83297eec66eeee8e579d82ab598da2/Library/Formula/llvm.rb --HEAD --with-clang --with-libcxx --disable-assertions`
2. checkout this repository and `cd $repo_dir`
3. `$ carton install`
  * NOTE: this command is probably failure. but to install dependency modules is success.
4. `$ make hello`
5. `$ ./hello`

Author
------

Naoki OKAMURA (Nyarla) *nyarla[ at ]thotep.net*

Public domain
-------------

These code is under the public domain.

So, you can enjoy new Perl world !
