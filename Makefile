run: hello.bc
	lli hello.bc

hello: hello.s
	gcc -o hello hello.s

hello.s: hello.bc
	llc -o hello.s hello.bc

hello.bc: hello.ll
	llvm-as hello.ll

hello.ll:
	bin/compile.pl src/hello.pl hello.ll

